import React, { useState, useEffect, useCallback, useRef } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import Button, { OutlineButton } from "../button/Button";
import Input from "../input/Input";
import { category } from "../../api/tmdbApi";
import axios from "axios";

import SearchRoundedIcon from "@mui/icons-material/SearchRounded";
import PersonIcon from "@mui/icons-material/Person";
import KeyIcon from "@mui/icons-material/Key";
import EmailIcon from "@mui/icons-material/Email";

import { Box, Modal, styled, Typography, IconButton } from "@mui/material";

import "./Header.scss";

const StyledModal = styled(Modal)({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

const Header = ({ user, user2 }) => {
  const headerRef = useRef(null);
  const [login, setLogin] = useState(false);
  const [signUp, setSignUp] = useState(false);
  const [data, setData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
  });

  const [dataLogin, setDataLogin] = useState({ email: "", password: "" });
  const [error, setError] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    const shrinkHeader = () => {
      if (
        document.body.scrollTop > 100 ||
        document.documentElement.scrollTop > 100
      ) {
        headerRef.current.classList.add("shrink");
      } else {
        headerRef.current.classList.remove("shrink");
      }
    };
    window.addEventListener("scroll", shrinkHeader);
    return () => {
      window.removeEventListener("scroll", shrinkHeader);
    };
  }, []);

  const handleLogin = () => {
    setLogin(true);
  };
  const handleSignUp = () => {
    setSignUp(true);
  };

  const google = () => {
    window.open("http://localhost:5000/auth/google", "_self");
  };

  const logout = () => {
    localStorage.removeItem("token");
    window.location.reload();
    window.open("http://localhost:5000/auth/logout", "_self");
  };

  const handleChange = ({ currentTarget: input }) => {
    setData({ ...data, [input.name]: input.value });
  };

  const handleSubmit = async (e) => {
    try {
      const url = "http://localhost:8080/api/users";
      const { data: res } = await axios.post(url, data);
      // navigate("/home");
      setSignUp(false);

      console.log(res.message);
    } catch (error) {
      if (
        error.response &&
        error.response.status >= 400 &&
        error.response.status <= 500
      ) {
        setError(error.response.data.message);
      }
    }
  };

  const handleChangeLogin = ({ currentTarget: input }) => {
    setDataLogin({ ...dataLogin, [input.name]: input.value });
  };

  const handleSubmitLogin = async (e) => {
    // e.preventDefault();
    try {
      const url = "http://localhost:8080/api/auth";
      const { data: res } = await axios.post(url, dataLogin);
      localStorage.setItem("token", res.data);

      window.location = "/";
    } catch (error) {
      if (
        error.response &&
        error.response.status >= 400 &&
        error.response.status <= 500
      ) {
        setError(error.response.data.message);
      }
    }
  };

  return (
    <div ref={headerRef} className="header">
      <div className="header__wrap container">
        <div className="logo">
          <Link to="/">MovieList</Link>
        </div>
        <div className="header_search">
          <MovieSearch />
        </div>

        {user && !user2 ? (
          <ul className="list">
            <li className="listItem">{user.displayName}</li>

            <OutlineButton onClick={logout} className="outlineBtnsSmall">
              Logout
            </OutlineButton>
          </ul>
        ) : user2 && !user ? (
          <ul className="list">
            <li className="listItem">
              <PersonIcon fontSize="large" />
            </li>

            <OutlineButton onClick={logout} className="outlineBtnsSmall">
              Logout
            </OutlineButton>
          </ul>
        ) : (
          <ul className="header__nav">
            <OutlineButton onClick={handleLogin} className="outlineBtnsSmall">
              Login
            </OutlineButton>
            <Button onClick={handleSignUp} className="small">
              Sign Up
            </Button>
          </ul>
        )}
      </div>
      <StyledModal
        open={login}
        onClose={(e) => setLogin(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            p: 2,
            border: "1px solid grey",
            backgroundColor: "#0f0f0f",
            width: "55vw",
            borderRadius: 1,
          }}
        >
          <Typography
            variant="h5"
            fontWeight={600}
            textAlign="center"
            marginBottom="1rem"
          >
            Login
          </Typography>
          <div className="login" style={{ marginBottom: "1rem" }}>
            <Input
              type="email"
              placeholder="Email"
              name="email"
              onChange={handleChangeLogin}
              value={dataLogin.email}
              required
            />
            <div style={{ position: "relative", flexShrink: 0 }}>
              <IconButton>
                <PersonIcon className="icon" />
              </IconButton>
            </div>
          </div>

          <div className="login" style={{ marginBottom: "1rem" }}>
            <Input
              type="password"
              placeholder="Password"
              name="password"
              onChange={handleChangeLogin}
              value={dataLogin.password}
              required
            />
            <div style={{ position: "relative", flexShrink: 0 }}>
              <IconButton>
                <KeyIcon className="icon" />
              </IconButton>
            </div>
          </div>

          {error && <div>{error}</div>}

          <OutlineButton
            onClick={handleSubmitLogin}
            className="outlineBtnsSmall"
          >
            Login
          </OutlineButton>
          <OutlineButton className="outlineBtnsLogin" onClick={google}>
            Google
          </OutlineButton>
        </Box>
      </StyledModal>

      <StyledModal
        open={signUp}
        onClose={(e) => setSignUp(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            p: 2,
            border: "1px solid grey",
            backgroundColor: "#0f0f0f",
            width: "55vw",
            borderRadius: 1,
          }}
        >
          <Typography
            variant="h5"
            fontWeight={600}
            textAlign="center"
            marginBottom="1rem"
          >
            Sign Up
          </Typography>
          <div className="login" style={{ marginBottom: "1rem" }}>
            <Input
              type="text"
              placeholder="First Name"
              name="firstName"
              onChange={handleChange}
              value={data.firstName}
              required
            />
            <div style={{ position: "relative", flexShrink: 0 }}>
              <IconButton>
                <PersonIcon className="icon" />
              </IconButton>
            </div>
          </div>
          <div className="login" style={{ marginBottom: "1rem" }}>
            <Input
              type="text"
              placeholder="Last Name"
              name="lastName"
              onChange={handleChange}
              value={data.lastName}
              required
            />
            <div style={{ position: "relative", flexShrink: 0 }}>
              <IconButton>
                <PersonIcon className="icon" />
              </IconButton>
            </div>
          </div>
          <div
            className="login"
            style={{ marginBottom: "1rem", borderColor: "white" }}
          >
            <Input
              type="email"
              placeholder="Email"
              name="email"
              onChange={handleChange}
              value={data.email}
              required
            />
            <div style={{ position: "relative", flexShrink: 0 }}>
              <IconButton>
                <EmailIcon className="icon" />
              </IconButton>
            </div>
          </div>

          <div className="login" style={{ marginBottom: "1rem" }}>
            <Input
              type="password"
              placeholder="Password"
              name="password"
              onChange={handleChange}
              value={data.password}
              required
            />

            <div style={{ position: "relative", flexShrink: 0 }}>
              <IconButton>
                <KeyIcon className="icon" />
              </IconButton>
            </div>
          </div>

          {/* <div
            className="login"
            style={{ marginBottom: "1rem", borderColor: "white" }}
          >
            <Input
              type="text"
              placeholder="Enter Password Confirmation"

              // value={keyword}
              // onChange={(e) => setKeyword(e.target.value)}
            />
            <div style={{ position: "relative", flexShrink: 0 }}>
              <IconButton>
                <KeyIcon className="icon" />
              </IconButton>
            </div>
          </div> */}
          {error && <div>{error}</div>}
          <OutlineButton onClick={handleSubmit} className="outlineBtnsSmall">
            Sign Up
          </OutlineButton>
        </Box>
      </StyledModal>
    </div>
  );
};

const MovieSearch = (props) => {
  const navigate = useNavigate();

  const [keyword, setKeyword] = useState(props.keyword ? props.keyword : "");

  const goToSearch = useCallback(() => {
    if (keyword.trim().length > 0) {
      navigate(`/${category.movie}/search/${keyword}`);
    }
  }, [keyword, navigate]);

  useEffect(() => {
    const enterEvent = (e) => {
      e.preventDefault();
      if (e.keyCode === 13) {
        goToSearch();
      }
    };
    document.addEventListener("keyup", enterEvent);
    return () => {
      document.removeEventListener("keyup", enterEvent);
    };
  }, [keyword, goToSearch]);

  return (
    <div className="movie-search">
      <Input
        type="text"
        placeholder="Enter keyword"
        value={keyword}
        onChange={(e) => setKeyword(e.target.value)}
      />
      <IconButton>
        <SearchRoundedIcon className="icon" />
      </IconButton>
    </div>
  );
};

export default Header;
